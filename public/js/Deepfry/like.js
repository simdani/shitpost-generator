var deepfryURL = '/shitpost-generator/deepfried';
// PRODUCTION URL
//var deepfryURL = '/deepfried';

$(document).ready(function(){
    // when the user clicks on like
    $('.like').on('click', function(){
        var deepfryid = $(this).data('id');
        var userid = $(this).data('user');
        $deepfryVote = $(this);

        $.ajax({
            url: deepfryURL + '/like',
            cache: false,
            type: 'POST',
            data: {
                'liked': 1,
                'deepfriedId': deepfryid,
                'userid': userid
            },

            success: function(response){

                $deepfryVote.parent().find('span.likes_count').text(response + " likes");
                $deepfryVote.addClass('hide');
                $deepfryVote.siblings().removeClass('hide');

            }
        });
    });

    // when the user clicks on unlike
    $('.unlike').on('click', function(){

        var deepfryid = $(this).data('id');
        var userid = $(this).data('user');
        $deepfryVote = $(this);

        $.ajax({
            url: deepfryURL + '/unlike',
            cache: false,
            type: 'POST',
            data: {
                'unliked': 1,
                'deepfriedId': deepfryid,
                'userid': userid
            },
            success: function(response){

                $deepfryVote.parent().find('span.likes_count').text(response + " likes");
                $deepfryVote.addClass('hide');
                $deepfryVote.siblings().removeClass('hide');

            }
        });

    });
});

