<?php
    // PDO duomenu bazes sukurimas, prijungimas pie mysql, priskirt reiksmes, grazint rezultataus ir t.t.
    class Database {
        private $host = DB_HOST;
        private $user = DB_USER;
        private $pass = DB_PASS;
        private $dbname = DB_NAME;

        private $dbh;
        private $stmt;
        private $error;

        public function __construct() {
            // Sukurt dsn
            $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
            $options = array(
                PDO::ATTR_PERSISTENT => true,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            );

            // Sukurt nauja PDO objekta
            try {
                $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
            } catch (PDOException $e) {
                $this->error = $e->getMessage();
                echo $this->error;
            }
        }

        // Prepare statement
        public function query($sql) {
            $this->stmt = $this->dbh->prepare($sql);
        }

        // priskirti uzklausai reiksmes
        public function bind($param, $value, $type = null) {
            if (is_null($type)) {
                switch (true) {
                    case is_int($value):
                        $type = PDO::PARAM_INT;
                        break;
                    case is_bool($value):
                        $type = PDO::PARAM_BOOL;
                        break;
                    case is_null($value):
                        $type = PDO::PARAM_NULL;
                        break;
                    default:
                        $type = PDO::PARAM_STR;
                }
            }

            $this->stmt->bindValue($param, $value, $type);
        }

        // Vykdyti prepared statement
        public function execute() {
            return $this->stmt->execute();
        }

        // Gauti rezultatus kaip masyva
        public function getResults() {
            $this->execute();
            return $this->stmt->fetchAll();
        }

        // gauti viena eilute kaip masyva
        public function singleResult() {
            $this->execute();
            return $this->stmt->fetch();
        }

        // gauti eiluciu kieki
        public function getRowCount() {
            return $this->stmt->rowCount();
        }
    }