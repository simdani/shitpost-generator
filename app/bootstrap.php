<?php
    // Ikraunam bibliotekas, helperius, kuriuos norim naudoti svetaineje
    // Ikrauti Config kintamuosius
    require_once 'config/config.php';

    // load helpers
    require_once 'helpers/url_helper.php';
    require_once 'helpers/session_helper.php';
    require_once 'helpers/clickbait_helper.php';
    require_once 'helpers/deepfried_helper.php';
    require_once 'helpers/emoji_helper.php';
//require_once 'libraries/Controller.php';
//require_once 'libraries/Core.php';
//require_once 'libraries/Database.php';

    // Ikrauti bibliotekas
    // Autoload Core libraries
    spl_autoload_register(function ($className){
        require_once 'libraries/' . $className . '.php';
    });