<?php require APPROOT . '/views/includes/header.php'; ?>

<div class="row">
    <div class="col-md-4 mx-auto">
        <div class="card card-body bg-light mt-4">

            <?php flash('register_success'); ?>

            <h2>Login</h2>

            <form action="<?php echo URLROOT;?>/users/login" method="post">

                <div class="form-group">
                    <label for="username">Username: </label>
                    <input type="text" name="username" class="form-control form-control-lg <?php echo (!empty($data['username_error'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['username']; ?>">
                    <span class="invalid-feedback"><?php echo $data['username_error']; ?></span>
                </div>

                <div class="form-group">
                    <label for="password">Password: </label>
                    <input type="password" name="password" class="form-control form-control-lg <?php echo (!empty($data['password_error'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['password']; ?>">
                    <span class="invalid-feedback"><?php echo $data['password_error']; ?></span>
                </div>

                <input type="submit" value="Login" class="btn btn-primary btn-block">

            </form>
        </div>
    </div>
</div>

<?php require APPROOT . '/views/includes/footer.php'; ?>
