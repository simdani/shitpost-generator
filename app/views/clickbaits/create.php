<?php require APPROOT . '/views/includes/header.php'; ?>



<a href="<?php echo URLROOT; ?>/clickbaits/index/1" class="btn btn-info">Back</a>

<div class="card card-body mt-4">

  <h1><?php echo $data['title']; ?></h1>

  <h3>Post your own clickbait</h3>

    <form action="<?php echo URLROOT;?>/clickbaits/create" method="post">

      <div class="form-group">
        <label for="clickbait">Clickbait title: <sup>*</sup></label>        
        <input type="text" name="clickbait" class="form-control form-control-lg <?php echo (!empty($data['clickbait_error'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['clickbait']; ?>">
        <span class="invalid-feedback"><?php echo $data['clickbait_error']; ?></span>
      </div>

      <input type="submit" class="btn btn-success" value="Post">

    </form>
</div>




<?php require APPROOT . '/views/includes/footer.php'; ?>
