<?php require APPROOT . '/views/includes/header.php'; ?>

<h1><?php echo $_SESSION['user_username']?> Emojified Texts</h1>


    <a class="btn btn-success text-white" href="<?php echo URLROOT; ?>/emojis/index/1">Back</a>


<br/>
<br/>

    <?php foreach ($data['emojifiedTexts'] as $emojifiedText) : ?>
    
    <div class="card">
        <div class="card-body">
        <h6 class="card-subtitle mb-2 text-muted">Created at <?php echo $emojifiedText['created_at']; ?></h6>
            <?php echo $emojifiedText['emojifiedText']; ?>

            <div class="pull-right">
                <a class="btn btn-primary text-white" href="<?php echo URLROOT; ?>/emojis/show/<?php echo $emojifiedText['id'];?>">Show</a>
            </div>
        </div>       
    </div>

    <br>

    <?php endforeach; ?>


<?php require APPROOT . '/views/includes/footer.php'; ?>
