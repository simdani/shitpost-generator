<?php require APPROOT . '/views/includes/header.php'; ?>
<h1><?php echo $data['title']; ?></h1>

<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=9; text/html; charset=utf-8">
    </head>
    <body>
<div class="card card-body mt-4">
    
      <div class="pull-right">
          <?php if (isLoggedIn()) : ?>
    <a class="btn btn-success text-white" href="<?php echo URLROOT; ?>/candle/create">Create</a>
          <?php endif; ?>
    <a class="btn btn-primary text-white" href="<?php echo URLROOT; ?>/candle/generate">Generate</a>
          </div>
        </div>
<?php require APPROOT . '/views/includes/footer.php'; ?>
    </body>
</html>

