<?php require APPROOT . '/views/includes/header.php'; ?>

<h1><?php echo $_SESSION['user_username']?> images</h1>


    <a class="btn btn-success text-white" href="<?php echo URLROOT; ?>/deepfried/index/1">Back</a>


<br/>
<br/>

    <?php foreach ($data['deepfried'] as $deepfried) : ?>
    
    <div class="card">
        <div class="card-body">
        <h6 class="card-subtitle mb-2 text-muted">Created at <?php echo $deepfried['created_at']; ?></h6>
            <img src="<?php echo URLROOT."/public/img/".$deepfried['path']; ?>">

            <div class="pull-right">
                <a class="btn btn-primary text-white" href="<?php echo URLROOT; ?>/deepfried/show/<?php echo $deepfried['id'];?>">Show</a>
            </div>
        </div>       
    </div>

    <br>

    <?php endforeach; ?>


<?php require APPROOT . '/views/includes/footer.php'; ?>
