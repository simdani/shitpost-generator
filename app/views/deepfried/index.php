<style>
    img{
        Max-width: 100%;
        height:auto;
    }
</style>

<?php require APPROOT . '/views/includes/header.php'; ?>

<?php flash('deepfry_message'); ?>

<h1><?php echo $data['title']; ?></h1>

<?php if (isLoggedIn()) : ?>

    <a class="btn btn-success text-white" href="<?php echo URLROOT; ?>/deepfried/create">Upload</a>

<?php endif; ?>

    <div class="pull-right">
        <?php if (isLoggedIn()) : ?>
            <a class="btn btn-dark" href="<?php echo URLROOT; ?>/deepfried/showMyDeepfried">My Images</a>
        <?php endif; ?>   
    </div>

    <a class="btn btn-primary text-white" href="<?php echo URLROOT; ?>/deepfried/generate">Editor</a>
    <a class="btn btn-primary text-white" href="<?php echo URLROOT; ?>/deepfried/jpeg">JPEG</a>

<br/>
<br/>

    <?php foreach ($data['deepfried'] as $deepfried) : ?>
    <div class="card">
        <div class="card-body">
        <h6 class="card-subtitle mb-2 text-muted">Submitted by <a href="<?php echo URLROOT; ?>/users/show/<?php echo $deepfried['userId']; ?>"><strong><?php echo $deepfried['username'];?></strong></a> at <?php echo $deepfried['deepfriedCreated']; ?></h6>
            <img src="<?php echo URLROOT."/public/img/".$deepfried['deepfriedPath']; ?>">
                <div class="DeepfryVote">
                <?php if (isLoggedIn()) : ?> 
                    <?php if (checkIfuserLikedDeepfried($_SESSION['user_id'], $deepfried['deepfriedId']) == 1 ): ?>
                        <span class="unlike fa fa-heart" data-user="<?php echo $_SESSION['user_id']; ?>" data-id="<?php echo $deepfried['deepfriedId']; ?>"></span>
                        <span class="like hide fa fa-heart-o" data-user="<?php echo $_SESSION['user_id']; ?>" data-id="<?php echo $deepfried['deepfriedId']; ?>"></span>
                    <?php elseif (checkIfuserLikedDeepfried($_SESSION['user_id'], $deepfried['deepfriedId']) == 0): ?>
                    <!-- user has not yet liked post -->
                        <span class="like fa fa-heart-o" data-user="<?php echo $_SESSION['user_id']; ?>" data-id="<?php echo $deepfried['deepfriedId']; ?>"></span>
                        <span class="unlike hide fa fa-heart" data-user="<?php echo $_SESSION['user_id']; ?>" data-id="<?php echo $deepfried['deepfriedId']; ?>"></span>
                    <?php endif ?>
                    
                    <?php endif; ?>
                    <span class="likes_count"><?php echo $deepfried['likes']; ?> likes</span>
                </div>


            <div class="pull-right">               
                <a class="btn btn-primary text-white" href="<?php echo URLROOT; ?>/deepfried/show/<?php echo $deepfried['deepfriedId'];?>">Show</a>

                <?php if(isset($_SESSION['user_id']) && $deepfried['user_id'] == $_SESSION['user_id']) : ?>
                    <form class="pull-right" action="<?php echo URLROOT;?>/deepfried/delete/<?php echo $deepfried['deepfriedId'];?>" method="post">
                        <input type="submit" value="Delete" class="btn btn-danger">
                    </form>
                <?php endif; ?>

            </div>
        </div>       
    </div>

    <?php endforeach; ?>

    <br>

    <ul class="pagination">
        <?php for($i = 1; $i <= $data['pages']; $i++): ?>
            <li class="page-item <?php if ($data['curentPage'] === $i) { echo ' active';} ?>"><a class="page-link" href="<?php echo URLROOT; ?>/deepfried/index/<?php echo $i;?>"><?php echo $i; ?></a></li>
        <?php endfor; ?>
    </ul>


<?php require APPROOT . '/views/includes/footer.php'; ?>

<script type="text/javascript" src="<?php echo URLROOT; ?>/public/js/Deepfry/like.js"></script>