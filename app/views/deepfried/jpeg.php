<?php require APPROOT . '/views/includes/header.php'; ?>
<style>
    #hidden{
        display:none;
    }

    #unhidden{
        display:block;
    }

    #advancedh{
        display:none;
    }

    label{
        min-width:80px;
        margin-left:10px;
    }

    button{
        margin:10px;
        width:150px;
    }

</style>

<a href="<?php echo URLROOT; ?>/deepfried/index/1" class="btn btn-info">Back</a>

<div class="card card-body mt-4">

    <h1><?php echo $data['title']; ?></h1>

        <div class="col-lg-12">
            <img id="in" style="display:none;">
            <img id="out">
        </div>
        <div class="col-lg-12" id="hidden">
            <div class="col-lg-6">
                Base JPEG compression:
                <input id="factor" type="range" min="0" max=".5" step=".01" value=".2" onchange="document.getElementById('fval').innerHTML=this.value" /><label id="fval">.2</label>
            </div>

            <nav>
                <button onClick="artifacts()" class="btn btn-success">Create Artifacts</button>
                <button onClick="reset()" id="resetbtn" class="btn btn-success">Reset Photo</button>
                <button onClick="save()" id="savebtn" class="btn btn-success">Save Image</button>
            </nav>

        </div>

    <input type="file" name="pic" id="pic" accept="image/">

    <br>

</div>
<?php require APPROOT . '/views/includes/footer.php'; ?>

<script type="text/javascript" src="<?php echo URLROOT; ?>/public/js/Deepfry/jpeg.js"></script>