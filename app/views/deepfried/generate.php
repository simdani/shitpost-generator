<?php require APPROOT . '/views/includes/header.php'; ?>
<style>
    #hidden{
        display:none;
    }

    #unhidden{
        display:block;
    }

    #advancedh{
        display:none;
    }

    label{
        min-width:80px;
        margin-left:10px;
    }

    button{
        margin:10px;
        width:150px;
    }

    .output {
        position: relative;
    }
    .placeholder {
        position: absolute;
        top: 0;
    }

    img.placeholder {
        visibility: hidden;
    }

    #canvas {
        cursor: url(<?php echo URLROOT ?>/public/paintbrush-cursor.cur) 16 16, crosshair !important;
    }

</style>

  <a href="<?php echo URLROOT; ?>/deepfried/index/1" class="btn btn-info">Back</a>

  <div class="card card-body mt-4">

    <h1><?php echo $data['title']; ?></h1>

    <h6>More functions coming soon</h6>
<div id="hidden">


        <div class="output">
            <canvas id="canvas" name="canvas"></canvas>
            <img id="image" class="placeholder" src="/shitpost-generator/public/img/test.png" />
        </div>
        <div class="col-lg-12">
            <label for="brush">Brush size</label>
            <input id="brush" name="brush" type="range" min="0" max="100" value="30">
        </div>
        <div class="col-lg-12">
            <div class="col-lg-6">
                <label for="hue">Hue</label>
                <input id="hue" name="hue" type="range" min="0" max="300" value="0">
                <label for="contrast">Contrast</label>
                <input id="contrast" name="contrast" type="range" min="-20" max="20" value="0">
                <label for="brightness">Brightness</label>
                <input id="brightness" name="brightness" type="range" min="-100" max="100" value="0">
                <label for="clip">Clip</label>
                <input id="clip" name="clip" type="range" min="0" max="100" value="0">

            </div>
            <div class="col-lg-6">
                <label for="vibrance">Vibrance</label>
                <input id="vibrance" name="vibrance" type="range" min="0" max="400" value="0">
                <label for="sepia">Sepia</label>
                <input id="sepia" name="sepia" type="range" min="0" max="100" value="0">
                <label for="gamma">Gamma</label>
                <input id="gamma" name="gamma" type="range" min="0" max="10" value="1" step="0.1">
                <label for="noise">Noise</label>
                <input id="noise" name="noise" type="range" min="0" max="120" value="0">
            </div>

            <nav>
                <button id="resetbtn" class="btn btn-success">Reset Photo</button>
                <button id="savebtn" class="btn btn-success">Save Image</button>
            </nav>

                <button id="vintagebtn" class="btn btn-primary">Vintage</button>
                <button id="lomobtn" class="btn btn-primary">Lomo</button>
                <button id="embossbtn" class="btn btn-primary">Emboss</button>
                <button id="tiltshiftbtn" class="btn btn-primary">Tilt Shift</button>
                <button id="radialblurbtn" class="btn btn-primary">Radial Blur</button>
                <button id="edgeenhancebtn" class="btn btn-primary">Edge Enhance</button>



                <button id="posterizebtn" class="btn btn-primary">Posterize</button>
                <button id="claritybtn" class="btn btn-primary">Clarity</button>
                <button id="orangepeelbtn" class="btn btn-primary">Orange Peel</button>
                <button id="sincitybtn" class="btn btn-primary">Sin City</button>
                <button id="sunrisebtn" class="btn btn-primary">Sun Rise</button>
                <button id="crossprocessbtn" class="btn btn-primary">Cross Process</button>



                <button id="hazydaysbtn" class="btn btn-primary">Hazy</button>
                <button id="lovebtn" class="btn btn-primary">Love</button>
                <button id="grungybtn" class="btn btn-primary">Grungy</button>
                <button id="jarquesbtn" class="btn btn-primary">Jarques</button>
                <button id="pinholebtn" class="btn btn-primary">Pin Hole</button>
                <button id="oldbootbtn" class="btn btn-primary">Old Boot</button>
                <button id="glowingsunbtn" class="btn btn-primary">Glow Sun</button>



                <button id="hdrbtn" class="btn btn-primary">HDR Effect</button>
                <button id="oldpaperbtn" class="btn btn-primary">Old Paper</button>
                <button id="pleasantbtn" class="btn btn-primary">Pleasant</button>

        </div>

</div>

          <input type="file" name="pic" id="pic" accept="image/">

    <br>





  </div>
<?php require APPROOT . '/views/includes/footer.php'; ?>

<script type="text/javascript" src="<?php echo URLROOT; ?>/public/js/camanJS/dist/caman.full.min.js"></script>
<script type="text/javascript" src="<?php echo URLROOT; ?>/public/js/Deepfry/edit.js"></script>
<script type="text/javascript" src="<?php echo URLROOT; ?>/public/js/Deepfry/liquify.js"></script>