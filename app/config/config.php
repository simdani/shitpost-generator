<?php
    // duombazes parametrai
    define('DB_HOST', 'localhost');
    define('DB_USER', 'root');
    define('DB_PASS', '');
    define('DB_NAME', 'shitpost-generator');

    // production img upload
    //define('IMG_UPLOAD_ROOT', '/var/www/shitpost-generator/public/img/%s.%s');
    define('IMG_UPLOAD_ROOT', $_SERVER['DOCUMENT_ROOT'].'/shitpost-generator/public/img/%s.%s');

    define('UNROOT', "/var/www/shitpost-generator/public/img/");

    // APP pradzia (root)
    define('APPROOT', dirname(dirname(__FILE__)));

    // URL (koks yra svetaines domenas(localhost, shitpost.science);
    define('URLROOT', 'http://localhost/shitpost-generator');

    // Svetaines vardas
    define('SITENAME', 'Shitpost Generator');

    // Globalus kintamasis patikrinimam ar vartotojas yra administratorius
    define('adminLevel', 'admin');