<?php
    class Pages extends Controller {
        public function __construct() {
        }

        // Pagrindinis puslapis, kuri mato lankytojai
        public function index() {

            $logos = ['( ͡° ʖ̯ ͡°)', '(⁄ ͡⁄°⁄ ͜ʖ ͡⁄°⁄ )', '( ͡° ͜ʖ ͡°)', '( ͡⊙ ͜ʖ ͡⊙)',  
            '( ͡◉ ͜ʖ ͡◉)', '(͡°;͜ʖ͡;°)', 'ლ(́◉◞౪◟◉‵ლ)', '( ͡°; ͜ʖ ͡;°)',  '( ͠°; ͟ʖ ͡;°)',
            '(/ ͡°;/ ͜/ʖ/ ͡;°/)', '(´༎ຶ ͜ʖ ༎ຶ `)', '(╭☞ ͡~ ͜ʖ ͡°)╭☞', '\( ﾟ∀ ﾟ)/', '( ͠° ͟ʖ ͡°)', '( ° ͜ʖ °)',
            '( ͡~ ͜ʖ ͡°)', '( ͡° ᴥ ͡°✿)', '༼ つ  ͡° ͜ʖ ͡° ༽つ', '( ͡ᵔ ͜ʖ ͡ᵔ )', '(ง ͠° ͟ل͜ ͡°)ง', '( ͡°( ͡° ͜ʖ( ͡° ͜ʖ ͡°)ʖ ͡°) ͡°)',
            '(ಠ ͜ʖಠ)', 'ᕦ( ͡° ͜ʖ ͡°)ᕤ', '( ͡ಠ ʖ̯ ͡ಠ)', '¯\_(ツ)_/¯', '(づȌ╭╮Ȍ)づ', '(＊☉౪ ⊙｡)'];

            $randomLogo = $logos[array_rand($logos)];

            $data = [
                'title' => 'Welcome to the shitpost generator',
                'logo' => $randomLogo
            ];

            $this->view('pages/index', $data);
        }

    }